package uc.model.dto;

import uc.model.entity.Papel;

public class UsuarioDTO {


    private Long id;
    private String nome;
    private String sobrenome;
    private String email;    
    private Long idPapel;
    private Papel papel;

    
    public String getNome() {
    	return this.nome;
    }
    
    public String getSobrenome() {
    	return this.sobrenome;
    }
    
    public String getEmail() {
    	return this.email;
    } 
    
    protected UsuarioDTO() {}

    public UsuarioDTO(Long id,String nome, String sobrenome, String email, Long idPapel, Papel papel) {
        this.id = id;
    	this.nome = nome;
        this.sobrenome = sobrenome;
        this.email = email;
        this.idPapel = idPapel;
        this.papel = papel;
    }


	public void setNome(String nome) {
		// TODO Auto-generated method stub
		this.nome = nome;
	}

	public Long getId() {
		// TODO Auto-generated method stub
		return this.id;
	}

	public void setSobrenome(String sobrenome) {
		// TODO Auto-generated method stub
		this.sobrenome = sobrenome;
	}



	public void setEmail(String email) {
		// TODO Auto-generated method stub
		this.email = email;
		
	}


	public Long getIdPapel() {
		// TODO Auto-generated method stub
		return this.idPapel;
	}

	public Papel getPapel() {
		return papel;
	}

	public void setPapel(Papel papel) {
		this.papel = papel;
	}



}
