package uc.model.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Papel {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    private String nome;
    private String descricao;

    
    public Papel(String nome, String descricao) {
        this.nome = nome;
        this.descricao = descricao;

    }
    
    public String getNome() {
    	return this.nome;
    }
    
    public String getDescricao() {
    	return this.descricao;
    }   
    
    public Long getId() {
    	return this.id;
    } 

    protected Papel() {}



    @Override
    public String toString() {
        return String.format(
                "Papel [id=%d, nome='%s', descricao='%s']",
                id, nome, descricao);
    }




}
