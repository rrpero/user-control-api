package uc.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.UniqueConstraint;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Entity
public class Usuario {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    private String nome;
    private String sobrenome;
    @Column(unique=true)
    private String email;
    private String senha;
    
    private Long idPapel;
    

    @ManyToOne
    private Papel papel;
    
    public String getNome() {
    	return this.nome;
    }
    
    public String getSobrenome() {
    	return this.sobrenome;
    }
    
    public String getEmail() {
    	return this.email;
    } 
    
    public Papel getPapel() {
    	return this.papel;
    }     

    protected Usuario() {}

    public Usuario(String nome, String sobrenome, String email, String senha, Papel papel) {
        this.nome = nome;
        this.sobrenome = sobrenome;
        this.email = email;
        this.senha = senha;
        this.papel = papel;
    }

    @Override
    public String toString() {
        return String.format(
                "Customer[id=%d, Nome='%s', Sobrenome='%s', Email='%s', Senha='%s', papel='%s']",
                id, nome, sobrenome, email, senha, papel.getNome());
    }

	public void setNome(String nome) {
		// TODO Auto-generated method stub
		this.nome = nome;
	}

	public Long getId() {
		// TODO Auto-generated method stub
		return this.id;
	}

	public void setSobrenome(String sobrenome) {
		// TODO Auto-generated method stub
		this.sobrenome = sobrenome;
	}

	public void setPapel(Papel papel) {
		// TODO Auto-generated method stub
		this.papel = papel;
	}

	public void setEmail(String email) {
		// TODO Auto-generated method stub
		this.email = email;
		
	}

	public boolean matches(String senha, BCryptPasswordEncoder passwordEncoder) {
		// TODO Auto-generated method stub
		return passwordEncoder.matches(senha, this.senha);
	}


	public String getSenha() {
		// TODO Auto-generated method stub
		return this.senha;
	}

	public Long getIdPapel() {
		// TODO Auto-generated method stub
		return this.idPapel;
	}

	public void setSenha(String senha) {
		// TODO Auto-generated method stub
		this.senha=senha;
		
	}

}
