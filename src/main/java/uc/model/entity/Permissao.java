package uc.model.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Permissao {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    private String nome;
    private String descricao;



    protected Permissao() {}

    public Permissao(String nome, String descricao) {
        this.nome = nome;
        this.descricao = descricao;
    }
    
    public String getNome() {
    	return this.nome;
    }
    
    public String getDescricao() {
    	return this.descricao;
    }    

    @Override
    public String toString() {
        return String.format(
                "Permissao[id=%d, nome='%s', descricao='%s']",
                id, nome, descricao);
    }

}
