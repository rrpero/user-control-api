package uc.model.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class PermissaoPapel {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    
    @ManyToOne
    private Permissao permissao;
    
    @ManyToOne
    private Papel papel;


    public Permissao getPermissao() {
    	return this.permissao;
    }   
    
    protected PermissaoPapel() {}

    public PermissaoPapel(Permissao permissao, Papel papel) {
        this.permissao = permissao;
        this.papel = papel;
        
    }

    @Override
    public String toString() {
        return String.format(
                "PermissaoPapel[id=%d, permissao='%s', papel='%s']",
                id, permissao.getDescricao(),papel.getDescricao());
    }



}
