package uc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import uc.api.controller.UsuarioController;
import uc.model.entity.Papel;
import uc.model.entity.Permissao;
import uc.model.entity.PermissaoPapel;
import uc.model.entity.Usuario;
import uc.service.PapelRepository;
import uc.service.PermissaoPapelRepository;
import uc.service.PermissaoRepository;
import uc.service.UsuarioRepository;

@SpringBootApplication
public class Application {

	private static final Logger log = LoggerFactory.getLogger(Application.class);

	public static void main(String[] args) {
		SpringApplication.run(Application.class);
	}

	@Bean
	public CommandLineRunner demo(UsuarioRepository usrepository,
								PermissaoRepository prrepository,
								PapelRepository pprepository,
								PermissaoPapelRepository prpprepository,
								BCryptPasswordEncoder passwordEncoder) {
		return (args) -> {
			
			//Papeis
			Papel ppadmnistrador = new Papel("administrador","Administrador");
			Papel ppusuario = new Papel("usuario", "Usuário");
			Papel ppcadastro = new Papel("cadastro", "Cadastro");
			pprepository.save(ppadmnistrador);
			pprepository.save(ppusuario);
			pprepository.save(ppcadastro);
			
			//Permissoes		
			Permissao prconsulta = new Permissao("consulta", "Consulta");
			Permissao predicao = new Permissao("edicao", "Edição");
			Permissao prcadastro = new Permissao("cadastro", "Cadastro");
			Permissao prremocao = new Permissao("remocao", "Remoção");
			prrepository.save(prconsulta);
			prrepository.save(predicao);
			prrepository.save(prcadastro);
			prrepository.save(prremocao);
			
			//Permissoes de Papel
				//Papel Administrador todas permissoes
			PermissaoPapel prppadmconsulta = new PermissaoPapel(prconsulta,ppadmnistrador);
			PermissaoPapel prppadmedicao = new PermissaoPapel(predicao,ppadmnistrador);
			PermissaoPapel prppadmcadastro = new PermissaoPapel(prcadastro,ppadmnistrador);
			PermissaoPapel prppadmremocao = new PermissaoPapel(prremocao,ppadmnistrador);
			prpprepository.save(prppadmconsulta);
			prpprepository.save(prppadmedicao);
			prpprepository.save(prppadmcadastro);
			prpprepository.save(prppadmremocao);
				//Papel Cadastro com permissão consulta e cadastro
			
			PermissaoPapel prppcdrcadastro = new PermissaoPapel(prcadastro,ppcadastro);
			PermissaoPapel prppcdrconsulta = new PermissaoPapel(prconsulta,ppcadastro);
			prpprepository.save(prppcdrcadastro);
			prpprepository.save(prppcdrconsulta);
				//Papel Usuário com permissão consulta
			PermissaoPapel prppusrconsulta = new PermissaoPapel(prconsulta,ppusuario);
			prpprepository.save(prppusrconsulta);
			
			
			

			//Administradores
			usrepository.save(new Usuario("Jack", "Shephard","jackshephard@gmail.com",passwordEncoder.encode("senhajackshephard"),ppadmnistrador));
			usrepository.save(new Usuario("Desmond", "Hume","desmondhume@gmail.com",passwordEncoder.encode("senhadesmondhume"),ppadmnistrador));
			usrepository.save(new Usuario("Bejamin", "Linus","benjaminlinus@gmail.com",passwordEncoder.encode("senhabenjaminlinus"),ppadmnistrador));
			//Cadastros
			usrepository.save(new Usuario("Kate", "Austen","kateausten@gmail.com",passwordEncoder.encode("senhakateausten"),ppcadastro));
			usrepository.save(new Usuario("Charlie", "Pace","charliepace@gmail.com",passwordEncoder.encode("senhacharliepace"),ppcadastro));
			usrepository.save(new Usuario("Hugo", "Reyes","hugoreyes@gmail.com",passwordEncoder.encode("senhahugoreyes"),ppcadastro));
			//Usuarios
			usrepository.save(new Usuario("James", "Ford","jamesford@gmail.com",passwordEncoder.encode("senhajamesford"),ppusuario));
			usrepository.save(new Usuario("John", "Locke","johnlocke@gmail.com",passwordEncoder.encode("senhajohnlocke"),ppusuario));
			usrepository.save(new Usuario("Sayid", "Jarrah","sayidjarrah@gmail.com",passwordEncoder.encode("senhasayidjarrah"),ppusuario));
			usrepository.save(new Usuario("Jim", "Kwon","jimkwon@gmail.com",passwordEncoder.encode("senhajimkwon"),ppusuario));		
			usrepository.save(new Usuario("Sun", "Kwon","sunkwon@gmail.com",passwordEncoder.encode("senhasunkwon"),ppusuario));
			usrepository.save(new Usuario("Claire", "Littleton","clairelitteton@gmail.com",passwordEncoder.encode("clairelittleton"),ppusuario));
			usrepository.save(new Usuario("Mr", "Eko","mreko@gmail.com",passwordEncoder.encode("senhamreko"),ppusuario));
			usrepository.save(new Usuario("Juliet", "Burke","julietburke@gmail.com",passwordEncoder.encode("senhajulietburke"),ppusuario));
			usrepository.save(new Usuario("Richard", "Alpert","richardalpert@gmail.com",passwordEncoder.encode("senharichardalpert"),ppusuario));
			usrepository.save(new Usuario("Daniel", "Faraday","danielfaraday@gmail.com",passwordEncoder.encode("senhadanielfaraday"),ppusuario));
			usrepository.save(new Usuario("Charlotte", "Lewis","charlottelewis@gmail.com",passwordEncoder.encode("senhacharlottelewis"),ppusuario));
			usrepository.save(new Usuario("Frank", "Lapidus","franklapidus@gmail.com",passwordEncoder.encode("senhafranklapidus"),ppusuario));
			usrepository.save(new Usuario("Danielle", "Rousseau","daniellerousseau@gmail.com",passwordEncoder.encode("senhadaniellerousseau"),ppusuario));
			
			

		};
	}

	private void pegarPermissoesPapelFindAll(Logger log2, PermissaoPapelRepository prpprepository,
			Papel papel) {
		// fetch all Permissoes de Papel
		log.info("Permissoes de " +papel.getNome() + " found with findByPapel():");
		log.info("-------------------------------");
		for (PermissaoPapel prpp : prpprepository.findByPapel(papel)) {
			log.info(prpp.toString());
		}
		log.info("");
	}

	private void pegarPermissoesPapelFindAll(Logger log2, PermissaoPapelRepository prpprepository) {
		// fetch all Permissoes de Papel
		log.info("Permissoes de Papel found with findAll():");
		log.info("-------------------------------");
		for (PermissaoPapel prpp : prpprepository.findAll()) {
			log.info(prpp.toString());
		}
		log.info("");	
		
	}

}