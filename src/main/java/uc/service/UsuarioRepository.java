package uc.service;


import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;


import uc.model.dto.UsuarioDTO;
import uc.model.entity.Papel;
import uc.model.entity.Usuario;


public interface UsuarioRepository extends CrudRepository<Usuario, Long> {

	//@Query("Select u.nome, u.papel, u.sobrenome, u.email, u.id from Usuario u ")
	Page<Usuario> findAll(Pageable page);
	
	@Query("Select new uc.model.dto.UsuarioDTO(u.id, u.nome, u.sobrenome, u.email, u.papel.id,u.papel ) from Usuario u ")
	Page<UsuarioDTO> findAllDTO(Pageable page);
	
	@Query("Select new uc.model.dto.UsuarioDTO(u.id, u.nome, u.sobrenome, u.email, u.papel.id,u.papel ) from Usuario u where u.id = ?1 ")
	UsuarioDTO findOneDTO(Long id);	
	
	List<Usuario> findByNome(String nome);
    
    List<Usuario> findBySobrenome(String sobrenome);
    
    @Query("Select u from Usuario u where nome = ?1 and sobrenome = ?2")
    List<Usuario> findByNomeRSobrenome(String nome, String sobrenome);       
   
    List<Usuario> findByPapel(Papel papel);    
    
    @Query("Select count(u) from Usuario u where email = ?1 and senha = ?2")
    int doLogin(String email, String senha);
    
    @Query("Select u from Usuario u where email = ?1 and senha = ?2")
    Optional<Usuario> doLoginGetUsuario(String email, String senha);

    @Query("Select u from Usuario u where email = ?1 and senha = ?2")
	Iterable<Usuario> findByEmailESenha(String email, String senha);

    @Query("Select u from Usuario u where email = ?1")
	Optional<Usuario> findByEmail(String email);   
    
    
    
    
    
}