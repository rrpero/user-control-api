package uc.service;


import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import uc.model.entity.Papel;
import uc.model.entity.Permissao;
import uc.model.entity.Usuario;


public interface PapelRepository extends CrudRepository<Papel, Long> {

	List<Papel> findByNome(String nome);
	
	List<Papel> findByNome(String nome, Pageable pageable);

    

    
    
}