package uc.service;


import java.util.List;

import org.springframework.data.repository.CrudRepository;

import uc.model.entity.Papel;
import uc.model.entity.Permissao;
import uc.model.entity.PermissaoPapel;
import uc.model.entity.Usuario;


public interface PermissaoPapelRepository extends CrudRepository<PermissaoPapel, Long> {

    List<PermissaoPapel> findByPapel(Papel papel);
    

    
    
}