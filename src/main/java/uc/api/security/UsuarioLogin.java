package uc.api.security;

public class UsuarioLogin {
	
	private String email;
	private String senha;
	private String nome;
	private String accessToken;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
		
	}
	
	public String getAccessToken() {
		return this.accessToken;
	}
	

}
