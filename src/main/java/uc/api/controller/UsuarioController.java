package uc.api.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.validator.internal.engine.messageinterpolation.TermResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ch.qos.logback.core.net.SyslogOutputStream;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


import io.jsonwebtoken.Jwts;
import uc.api.security.TokenAuthenticationService;
import uc.api.security.UsuarioLogin;
import uc.api.util.CustomErrorType;
import uc.model.dto.UsuarioDTO;
import uc.model.entity.Papel;
import uc.model.entity.Permissao;
import uc.model.entity.PermissaoPapel;
import uc.model.entity.Usuario;
import uc.service.PapelRepository;
import uc.service.PermissaoPapelRepository;
import uc.service.UsuarioRepository;

@RestController
public class UsuarioController {

	static final String SECRET = TokenAuthenticationService.SECRET;
	static final String TOKEN_PREFIX = TokenAuthenticationService.TOKEN_PREFIX;
	static final String HEADER_STRING = TokenAuthenticationService.HEADER_STRING;    
    
    @Autowired
    private UsuarioRepository usrepository;
    
    @Autowired
    private PermissaoPapelRepository prpprepository;  

    @Autowired
    private PapelRepository pprepository; 
    
    @Autowired
    private PapelRepository prrepository;    
   
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;    
    


    /*
     * Listar Usuarios Paginado
     * Permissão - consulta
     * Método GET
     * Enderećo /usuarios
     * Parâmetros:
     *	-pagina int
     *	-tamanhoPagina int
     */
    @CrossOrigin
    @RequestMapping(value = "/usuarios", method = RequestMethod.GET)
    public ResponseEntity<Page<UsuarioDTO>> listarTodosUsuarios(HttpServletRequest  request,
    								@RequestParam(value="pagina", defaultValue="0") int pagina,
    								@RequestParam(value="tamanhoPagina", defaultValue="3") int tamanhoPagina) throws IOException {
    	
    	String token = request.getHeader(HEADER_STRING);
    	Sort sort = new Sort(Sort.Direction.ASC, "nome");
    	Pageable page = new PageRequest(pagina, tamanhoPagina,sort);
    	
    	Page<UsuarioDTO> usuariosPage = usrepository.findAllDTO(page);

		if(temPermissao(token, "consulta")) {
			return new ResponseEntity<Page<UsuarioDTO>>(usuariosPage, HttpStatus.OK);
		}
		else {
			return new ResponseEntity(new CustomErrorType("Não tem permissão a essa funcão."),
                    HttpStatus.FORBIDDEN);
		}
    }
 
    
    /*
     * Pegar dados do usuário 
     * Permissão - edicao ou o próprio usuário
     * Método GET
     * Enderećo /usuarios/{id}
     * 	-id long
     * 
     */    
    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = "/usuarios/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getUsuario(HttpServletRequest  request, @PathVariable("id") long id) {
       
    	String token = request.getHeader(HEADER_STRING);
    	
		if(temPermissao(token, "edicao") || dadosUsuarioLogado(token, id)) {		
			
			UsuarioDTO usuario = usrepository.findOneDTO(id);
			if(usuario == null) {
	            return new ResponseEntity<Object>(new CustomErrorType("Não foi possível recuperar o usuário de id: " + id + " pois não foi encontrado."),
	                    HttpStatus.NOT_FOUND);
			}
			return new ResponseEntity<UsuarioDTO>(usuario, HttpStatus.OK);
		}
		else {
			return new ResponseEntity<Object>(new CustomErrorType("Não tem permissão a essa funcão."),
                    HttpStatus.FORBIDDEN);
			
		}
    }    

    /*
     * Editar Usuario
     * Permissão - edicao ou o próprio usuário
     * Método POST
     * Enderećo /usuarios/{id}
     * 	-id long
     * Body:
     *	-Usuario usr
     */
    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(method = RequestMethod.PUT, value = "/usuarios/{id}")
    public ResponseEntity<?> editarUsuario(HttpServletRequest  request, HttpServletResponse response,
    						@RequestBody Usuario usr
    		) throws IOException {
    	
    	String token = request.getHeader(HEADER_STRING);
    	
    	//
		if(temPermissao(token, "edicao") || dadosUsuarioLogado(token, usr.getId())) {

			
			Usuario usuario = usrepository.findOne(usr.getId());
			if(usuario == null) {
	            return new ResponseEntity<Object>(new CustomErrorType("Não foi possível update no usuário de id: " + usr.getId() + " pois não foi encontrado."),
	                    HttpStatus.NOT_FOUND);
			}
			usuario.setNome(usr.getNome());
			usuario.setSobrenome(usr.getSobrenome());
			usuario.setEmail(usr.getEmail());
			if(!usr.getSenha().equals(""))
				usuario.setSenha(passwordEncoder.encode(usr.getSenha()));
			Papel papel = pprepository.findOne(usr.getIdPapel());

			usuario.setPapel(papel);			
			if(
					(
						temPapel(token, "cadastro") && 
						(papel.getNome().equals("administrador"))
					) || //é papel cadastro mandando editar para administrador
					(
						temPapel(token, "usuario") && 
						(
							papel.getNome().equals("administrador") || 
							papel.getNome().equals("cadastro")
						)
					)//é papel usuario mandando editar para administrador ou cadastro
				) 
				return new ResponseEntity<Object>(new CustomErrorType("Não tem permissão a essa funcão."),
                    HttpStatus.FORBIDDEN);

			usrepository.save(usuario);
			usuario = usrepository.findOne(usr.getId());
			return new ResponseEntity<UsuarioDTO>(new UsuarioDTO(usuario.getId(), usuario.getNome(), usuario.getSobrenome(), usuario.getEmail(), usuario.getIdPapel(), usuario.getPapel()), HttpStatus.OK);
		}
		else {
			return new ResponseEntity<Object>(new CustomErrorType("Não tem permissão a essa funcão."),
                    HttpStatus.FORBIDDEN);
			
		}
    } 
    
    /*
     * Cadastrar Usuario
     * Permissão - cadastro
     * Método POST
     * Enderećo /usuarios
     * Body:
     *	-Usuario usr
     */
    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(method = RequestMethod.POST, value = "/usuarios")
    public ResponseEntity<?> criarUsuario(HttpServletRequest  request, HttpServletResponse response,    					
											@RequestBody Usuario usr
    		) throws IOException {
    	
    	
    	String token = request.getHeader(HEADER_STRING);
    	Papel papelACadastrar = pprepository.findOne(usr.getIdPapel());
		if(temPermissao(token, "cadastro") && (temPapel(token, "administrador") //administrador edita tudo
				|| 							
				(temPapel(token, "cadastro") && !papelACadastrar.getNome().equals("administrador")) //cadastro nao pode cadastrar administrador
				)) {

			
			
			Usuario usuario = new Usuario(usr.getNome(), usr.getSobrenome(), usr.getEmail(), passwordEncoder.encode(usr.getSenha()), papelACadastrar);			
			usrepository.save(usuario);
			return new ResponseEntity<UsuarioDTO>(new UsuarioDTO(usuario.getId(), usuario.getNome(), usuario.getSobrenome(), usuario.getEmail(), usuario.getIdPapel(), usuario.getPapel()), HttpStatus.CREATED);
		}
		else {
			return new ResponseEntity<Object>(new CustomErrorType("Não tem permissão a essa funcão."),
                    HttpStatus.FORBIDDEN);
			
		}
    }      
 
    /*
     * Remover Usuario
     * Permissão - remocao e não é está removendo o próprio usuário logado
     * Método DELETE
     * Enderećo /usuarios/{id}
     * 	-id long
     */
    @CrossOrigin
    @RequestMapping(method = RequestMethod.DELETE, value = "/usuarios/{id}")
    public ResponseEntity<?> removerUsuario(HttpServletRequest  request, HttpServletResponse response,
    					@PathVariable("id") long id
    		) throws IOException {
    	
    	
    	String token = request.getHeader(HEADER_STRING);
    	
		if(temPermissao(token, "remocao") && !dadosUsuarioLogado(token, id)) {			
			
			Usuario usuario = usrepository.findOne(id);
			if(usuario == null) {
				return new ResponseEntity<Object>(new CustomErrorType("Não foi possível remover o usuário de id: " + id + " pois não foi encontrado."),
	                    HttpStatus.NOT_FOUND);
			}
			else {
				usrepository.delete(id);
				return new ResponseEntity<Usuario>(HttpStatus.NO_CONTENT);
			}
		}
		else {
			return new ResponseEntity<Object>(new CustomErrorType("Não tem permissão a essa funcão."),
                    HttpStatus.FORBIDDEN);
			
			
		}
    }       

    
    /*
     * Listar Papeis
     * Permissão - consulta
     * Método GET
     * Enderećo /papeis
     */    
    @CrossOrigin
    @RequestMapping(value = "/papeis", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Papel>>  listarTodosPapeis(HttpServletRequest  request, HttpServletResponse response) throws IOException {
    	
    	String token = request.getHeader(HEADER_STRING);
    	
		if(temPermissao(token, "consulta"))		
			return new ResponseEntity<Iterable<Papel>>(pprepository.findAll(), HttpStatus.OK);
		else {
			return new ResponseEntity(new CustomErrorType("Não tem permissão a essa funcão."),
                    HttpStatus.FORBIDDEN);
		}
    } 
    
    
    /*
     * Login
     * Permissão - aberto
     * Método POST
     * Endereco /login
     * Body:
     * -UsuarioLogin usuarioLogin
     */ 
    @CrossOrigin
	@RequestMapping(method = RequestMethod.POST, value = "/login")
    public ResponseEntity<?>  login(HttpServletResponse response,
    		//@RequestParam(value="email", defaultValue="") String email,
    		//@RequestParam(value="senha", defaultValue="") String senha
    		@RequestBody UsuarioLogin usuarioLogin
    		
    		) {

    	Optional<Usuario> usuario = usrepository.findByEmail(usuarioLogin.getEmail());
    	if(!usuario.isPresent())
    		return new ResponseEntity(new CustomErrorType("Não foi possível o login."),
                    HttpStatus.NOT_FOUND);

    	if( usuario.get().matches(usuarioLogin.getSenha(),passwordEncoder)){
    		Papel papel = pprepository.findOne(usuario.get().getPapel().getId());

    		List<String> permissoes = new ArrayList<String>();
    		for(PermissaoPapel prpp : prpprepository.findByPapel(papel))
    		{
    			permissoes.add(prpp.getPermissao().getNome());
    		}
    		TokenAuthenticationService.addAuthentication(response, usuario.get(),permissoes);
    		String accessToken = response.getHeader(HEADER_STRING).replace(TOKEN_PREFIX, "").trim();
    		usuarioLogin.setAccessToken(accessToken);
    		usuarioLogin.setSenha("");
    		return new ResponseEntity<UsuarioLogin>(usuarioLogin, HttpStatus.OK);
    	}
    	else return new ResponseEntity(new CustomErrorType("Não foi possível o login."),
                HttpStatus.NOT_FOUND);
    } 
	
	//Verifica se pela role o usuario tem permissao
    private boolean temPermissao(String token, String permissao) {
		String role = (String) Jwts.parser()
				.setSigningKey(SECRET)
				.parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
				.getBody().get("role");
		for(Papel papel: pprepository.findByNome(role)) {
			for(PermissaoPapel prpp: prpprepository.findByPapel(papel)) {				
				if(prpp.getPermissao().getNome().equals(permissao))
					return true;
			}
		}
		return false;
		
		
	}
    
    //Verifica se o pelo token tem role
    private boolean temPapel(String token, String papel) {
		String role = (String) Jwts.parser()
				.setSigningKey(SECRET)
				.parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
				.getBody().get("role");
		return role.equals(papel);
		
		
	}    
    

    //retorna o id do token
    private boolean dadosUsuarioLogado(String token, long id) {
		
    	int idToken = (int) Jwts.parser()
				.setSigningKey(SECRET)
				.parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
				.getBody().get("id");

    	return idToken == id;
		
		
	}     

    
}
